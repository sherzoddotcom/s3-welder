"""
A sample CLI app. Replace this message with something meaningful.
"""
import click
import boto3
import sys
from loguru import logger
from s3welder.core import S3Welder
from hashlib import md5
import glob


@click.group()
@click.option(
    "-v", "--verbose", count=True, help="Be verbose with output. -vv outputs debug info"
)
@click.pass_context
def cli(ctx, verbose):
    """Entry point to the click app. Normally you dont need to change anything here."""
    ctx.ensure_object(dict)
    logger.remove()
    if verbose == 1:
        logger.add(
            sys.stdout,
            level="INFO",
            format="<green>{time}</green> <level>{message}</level>",
        )
    elif verbose == 2:
        logger.add(
            sys.stdout,
            level="DEBUG",
            format="<green>{time}</green> <level>{message}</level>",
        )
    ctx.obj["app"] = S3Welder(boto3.client("s3"), logger)


@cli.command("list")
@click.argument("bucket")
@click.pass_context
def do_list(ctx, bucket):
    """List bucket objects"""
    try:
        objects = ctx.obj["app"].list_objects(bucket)
        [print(o["Key"]) for o in objects]
    except Exception as e:
        logger.error(e)


@cli.command("merge")
@click.pass_context
@click.argument("bucket")
@click.option("--final-object", default="merged")
def do_merge(ctx, bucket, final_object):
    """Merge objects in a bucket into a new object in the same bucket"""
    try:
        objects = ctx.obj["app"].list_objects(bucket)
        ctx.obj["app"].merge_objects(bucket, objects, final_object)
    except Exception as e:
        print(e)


@cli.command("checksum")
@click.argument("directory")
def do_checksum(directory):
    """ Calculate ETag for the files in the directory"""
    etag = md5()
    files = sorted(glob.glob(f"{directory}/*"))
    for file in files:
        logger.info(f"Checksuming file: {file}")
        with open(file, "rb") as f:
            file_md5 = md5(f.read())
            logger.debug(f"File checksum: {file_md5.hexdigest()}")
            etag.update(file_md5.digest())

    print("{}-{}".format(etag.hexdigest(), len(files)))


if __name__ == "__main__":
    cli(obj={})
