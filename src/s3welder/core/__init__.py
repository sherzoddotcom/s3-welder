#!/usr/bin/env python

"""
Merges objects in S3 bucket into one object
"""


class NullLogger:
    def info(self, log):
        pass

    def debug(self, log):
        pass


class S3Welder:
    def __init__(self, s3_client, logger=NullLogger()):
        self.client = s3_client
        self.logger = logger

    def list_objects(self, bucket):
        resp = self.client.list_objects_v2(Bucket=bucket)
        self.logger.info(f"Listing objects in bucket: {bucket}")
        self.logger.debug(resp)
        return resp.get("Contents", [])

    def merge_objects(self, bucket, objects, final_object):
        resp = self.client.create_multipart_upload(Bucket=bucket, Key=final_object)
        upload_id = resp["UploadId"]
        self.logger.info(
            f"Created multipart upload: {upload_id} for bucket: {bucket} key: {final_object}"
        )

        parts = []
        for idx, object in enumerate(sorted(objects, key=lambda x: x["Key"])):
            part_resp = self.client.upload_part_copy(
                Bucket=bucket,
                Key=final_object,
                PartNumber=idx + 1,
                UploadId=upload_id,
                CopySource={"Bucket": bucket, "Key": object["Key"]},
            )
            self.logger.info(f"Copied part{idx+1}")
            self.logger.info(part_resp["CopyPartResult"])
            parts.append(part_resp["CopyPartResult"])

        if parts:
            completion_resp = self.client.complete_multipart_upload(
                Bucket=bucket,
                Key=final_object,
                UploadId=upload_id,
                MultipartUpload={
                    "Parts": [
                        {"ETag": p["ETag"], "PartNumber": p_idx + 1}
                        for p_idx, p in enumerate(parts)
                    ]
                },
            )
            self.logger.info(f"Completed upload: {upload_id}")
            self.logger.debug(completion_resp)
            self.logger.info("Merge complete")
            return

        self.client.abort_multipart_upload(
            Bucket=bucket, Key=final_object, UploadId=upload_id
        )
        self.logger.info(f"Aborted multipart upload: {upload_id}")
