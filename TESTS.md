# Test setup

Tests require running localstack instance. Only S3 service is needed.

```bash
docker run --rm -it -p 4566:4566 -e SERVICES=s3 localstack/localstack
```
