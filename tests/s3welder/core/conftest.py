import pytest
import boto3

TEST_BUCKET = "testbucket"


@pytest.fixture(scope="session")
def test_bucket():
    yield TEST_BUCKET


@pytest.fixture(scope="session")
def s3_client():
    client = boto3.client(
        "s3",
        region_name="us-east-1",
        verify=False,
        endpoint_url="https://localhost:4566",
    )
    client.create_bucket(Bucket=TEST_BUCKET)
    yield client
    resp = client.list_objects_v2(Bucket=TEST_BUCKET)
    objects = [{"Key": k["Key"]} for k in resp["Contents"]]
    client.delete_objects(Bucket=TEST_BUCKET, Delete={"Objects": objects})
    client.delete_bucket(Bucket=TEST_BUCKET)
