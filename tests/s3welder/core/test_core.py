from s3welder.core import S3Welder
from hashlib import md5

MIN_SIZE = 6000000


def test_s3welder(s3_client, test_bucket):
    parts = {k: get_bytes(k) for k in "abc"}
    _ = [
        s3_client.put_object(Bucket=test_bucket, Body=c[0], Key=p)
        for p, c in parts.items()
    ]
    welder = S3Welder(s3_client)
    objects = welder.list_objects(test_bucket)
    assert len(objects) == len(parts)
    welder.merge_objects(test_bucket, objects, "finalobject")

    obj = s3_client.get_object(Bucket=test_bucket, Key="finalobject")
    expected_etag = calculate_etag([p[1] for p in parts.values()])
    assert f'"{expected_etag}"' == obj["ETag"]


def get_bytes(t):
    b = (t * MIN_SIZE).encode()
    m = md5()
    m.update(b)
    return b, m


def calculate_etag(parts):
    etag = md5()
    _ = [etag.update(p.digest()) for p in parts]
    return "{}-{}".format(etag.hexdigest(), len(parts))
