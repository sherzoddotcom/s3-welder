from s3welder.cli.app import cli


# TODO create localstack based test
def test_cli_list(cli_runner):
    result = cli_runner.invoke(cli, ["list", "somebucket"])
    # assert result.exit_code == 0
    assert "InvalidAccessKeyId" in result.output


def test_cli_merge(cli_runner):
    result = cli_runner.invoke(
        cli, ["merge", "somebucket", "--final-object", "final_object"]
    )
    # assert result.exit_code == 0
    assert "InvalidAccessKeyId" in result.output
