# S3 Welder

A CLI tool to combine S3 object parts into single object. Suitable for cases when an object can not be uploaded as a whole, and needs to be split and uploaded in parts.

## Use case

Consider a 500MB file. We have a AWS session token that can last only an hour and not enough to upload the whole file. Here is how we go about this:

- split the file into manageable parts, and put it into a folder
- sync the folder to S3, pause and resume whenever
- use the `s3-welder` and combine the parts in S3

## Usage
